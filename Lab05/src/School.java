public class School {
    public String name;
    public String shortName;

    public School() {

    }

    public School(String name){
        // constructor
        this.name = name;
        this.shortName = null;
    }
    
    public School(String name, String shortname) {
        this.name=name;
        this.shortName= shortname;
    }    

    
    public String getDomain() {
        return this.shortName + ".edu.vn";

    }
}
