import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;


import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

public class App {
    public static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) throws Exception {
        // Part3_Q2();
        Curency_Exchange();
        // Input_Request();
        // Curency_Exchange();
        // int [][] arr = {
        //     {1,2},
        //     {4,5},
        //     {7,8}
        // };
        // for (int i = 0; i < arr.length; i++) {
        //     for (int j = 0; j < arr.length; j++) {
        //         System.out.print(arr[i][j] + "\t");
        //     }
        //     System.out.println("\n");
        // }

    }

    // static void Test() {
    // double angleInRadians = Math.acos(1);
    // double angleInDegree = Math.toDegrees(angleInRadians);
    // System.out.println(angleInDegree);
    // 
    public static int Input_Request() {
        int value=0;
        do {
            System.out.println("Enter a positive integer:");
            String temp = sc.nextLine();
            value=Integer.parseInt(temp);
            try {
                if (value >= 0) {
                    break;
                }else {
                    System.out.println("Input positive value, please!:");
                    continue;
                }
            } catch (Exception e) {
                System.out.println("Entered wrong request");
                continue;
            }
        } while (true);
        
        return value;
    }
    public static boolean Classification(int a, int b, int c) {

        if (a < b + c && b < a + c && c < a + b) {
            if (a * a == b * b + c * c || b * b == a * a + c * c || c * c == a * a + b * b)
                System.out.println("This is Right Triangle");
            else if (a == b && b == c)
                System.out.println("This is Equalateral Triangle");
            else if (a == b || a == c || b == c)
                System.out.println("This is Isosceles Triangle");
            else if (a * a > b * b + c * c || b * b > a * a + c * c || c * c > a * a + b * b)
                System.out.println("This is Octuse Triangle");
            else
                System.out.println("This is Acute Triangle");

            return true;
        } else {
            System.out.println("Three edge you have gave me is not a part of a triangle");

            return false;
        }
    }

    public static void Acreage_Triangle() {
        var sc = new Scanner(System.in);
        System.out.println("Input: ");

        do {
            try {
                System.out.print("a = ");
                var in1 = sc.next();
                System.out.print("b = ");
                var in2 = sc.next();
                System.out.print("c = ");
                var in3 = sc.next();
                System.out.print("h = ");
                var in4 = sc.next();

                var a = Double.parseDouble(in1);
                var b = Double.parseDouble(in2);
                var c = Double.parseDouble(in3);
                var h = Double.parseDouble(in4);

                if (a > 0 && b > 0 && c > 0 && h > 0) {
                    if (a * a == b * b + c * c || b * b == a * a + c * c || c * c == a * a + b * b) {
                        System.out.println("This is Right Triangle");
                        double rsr = (a * b) / 2;
                        System.out.println("Your acreage is: " + rsr);
                    } else if (a == b && b == c) {
                        System.out.println("This is Equilateral Triangle: ");
                        double rse = (a * a * (Math.sqrt(3))) / 4;
                        System.out.println("Your acreage is: " + rse);
                    } else if (a == b || a == c || b == c) {
                        System.out.println("This is Isosceles Triangle");
                        double rs = (a * h) / 2;
                        System.out.println("Your acreage is: " + rs);
                    } else if (a * a > b * b + c * c || b * b > a * a + c * c || c * c > a * a + b * b) {
                        System.out.println("This is Octue Triangle");
                        double p = (a + b + c) / 2;
                        double rs = Math.sqrt(p * (p - a) * (p - b) * (p - c));
                        System.out.println("Your acreage is: " + rs);
                    } else {
                        System.out.println("This is an Acute");
                        double p = (a + b + c) / 2;
                        double rs = Math.sqrt(p * (p - a) * (p - b) * (p - c));
                        System.out.println("Your acreage is: " + rs);
                    }

                    // double rs = (a * h) / 2;
                    // double rse = (a*a * (Math.sqrt(3)))/4;
                    // double rsr = (a * b) / 2;
                    // double rst = (a * a) / 2;
                    // System.out.println("Your acreage of Nomal triangle is : " + rs);
                    // System.out.println("Your acreage of Equilateral Triangle is : " + rse);
                    // System.out.println("Your acreage of Right Triangle is : " + rsr);
                    // System.out.println("Your acreage of Triangle Right Angle is : " + rst);

                    break;
                } else {
                    System.out.println("Positive Integer, please: ");
                }

            } catch (Exception e) {
                System.out.println("Wrong input value, try again: ");
            }
        } while (true);
        sc.close();
        // return rs;
    }

    public static void Curency_Exchange() {        
        Scanner scanner = new Scanner(System.in);
        HashMap<String, Double> currency = new HashMap<>();
        String standard = "VND";
        currency.put("VND", 1.0);
        currency.put("USD", 23.174);
        currency.put("EUR", 26.977);    
        
        String keyOrigin = dependencyStringWithLength(scanner,"Tiền gì?",currency);
        double valueOrgin = dependencyReadDouble(scanner,"Bao nhiêu?");
        String convert = dependencyStringWithLength(scanner,"Chuyển sang tiền gì?",currency);

        double result = 0.0;

        if(!convert.equals(standard)){
            double s = valueOrgin * currency.get(keyOrigin);
            if(currency.get(standard) < currency.get(convert)){
                result = s / currency.get(convert);
            }else{
                result = s * currency.get(convert);
            }
        }else{
            result = valueOrgin * currency.get(keyOrigin);
        }

        System.out.println("Ket qua \t" + result);

        
    }

    public static double dependencyReadDouble(Scanner scanner, String ask){
        while (true) {
            System.out.print(ask);
            String value = scanner.nextLine();
            try {
                double result = Double.parseDouble(value);
                if (result <= 0) {
                    continue;
                } else {
                    return result;
                }
            } catch (Exception e) {
                System.out.print("Type input formatter was error!");
            }
        }

    }

    public static String dependencyStringWithLength(Scanner scanner,String ask,HashMap<String, Double> currency) {
        while (true) {
            System.out.print(ask);
            String value = scanner.nextLine();
            if (!isAlloweCurrency(currency, value) == false){
                continue;
            } else {
                return value;
            }
        }
    }

    public static boolean isAlloweCurrency(HashMap<String, Double> currency, String check){
        
        for (String key : currency.keySet()) {
            return check == key;
        }
        return false;

    }

    public static void Part3_Q1() {
        // var sc = new Scanner(System.in);
        // char f = sc.next().charAt(0);
        // System.out.println(f);
        var value = new ArrayList<String>();
        value.add("Maria");
        value.add("Alexa");
        value.add("Khalifa");
        value.add("Rae");
        value.add("Lucie");
        
        char test = 'M';
        for (String e : value ) {
            if (e.charAt(0) == test) {
                System.out.println(e);
            }
        }
    }


    public static void Part3_Q2() {
        String testString = "Hello_I_am_Iron_Man";
        // char[] stringToCharArray = testString.toCharArray();
        String[] split = testString.split("_");
 
		for (String string : split) {
			System.out.println(string);
        }
        /*split("regex") -> tách chuỗi theo ký tự regex và trả về mảng []
        * trim() => loại bỏ dấu cách dư thừa
        * replace() => thay thế một kí tự hoặc một chuỗi thành chuỗi khác
        * charAt(index) => trả về vị trí của một ký tự trong chuỗi
        * substring(beginIndex, endIndex)
        * substring(beginIndex) => cắt chuỗi từ một vị trí đến một vị trí được định sẵn
        * toUpperCase() => chữ in hoa
        * toLowerCase() => chữ in thường
        */
    }

    public static void Part3_Q3() {
        String stringArray[] = {"Hello", "_I", "_am", "_Iron", "_Man"};
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < stringArray.length; i++) {
            sb.append(stringArray[i]);
        }
        String str = sb.toString();
        System.out.println(str);
    }


    public static void Part3_Q4() {
        // File input = new File("input.txt");
        // Map<String, Object> jsonFileAsMap = new ObjectMapper().readValue(input, new
        // TypeReference<Map<String, Object>>() {});

        // Map<String, Object> ratesMap = (Map<String, Object>)
        // jsonFileAsMap.get("rates");
        // ratesMap.forEach((k, v) -> System.out.println("Key = " + k + ", Value = " +
        // v));
        // HashMap<Integer, String> hmap = new HashMap<>();
        // Scanner in = new Scanner(System.in);

        // for (int i = 0; i < 3; i++) {
        //     System.out.println("in1: ");
        //     Integer a = in.nextInt();
        //     System.out.println("in2: ");
        //     String b = in.next();

        //     hmap.put(a, b);
        // }

        // for (Entry<Integer, String> m : hmap.entrySet()) {
        //     System.out.println(m.getKey() + " " + m.getValue());
        // }
        // Map<Integer, String> hashmap = new HashMap<Integer, String>();
        // Scanner sc = new Scanner(System.in);
        // System.out.print("Shashank Pathak\nEnter number of character: ");
        // int n = sc.nextInt();
        // int[] a = new int[n];

        // for (int i = 0; i < a.length; i++) {
        //     Integer b = sc.nextInt();
        //     String c = sc.nextLine();

        //     hashmap.put(b, c);
        // }

        // for (Map.Entry<Integer, String> mp : hashmap.entrySet()) {
        //     System.out.println("\n" + mp.getKey() + " " + mp.getValue());
        // }
        Scanner in = new Scanner(System.in);
 
        HashMap<String, String> list = new HashMap<>();
        // Add random name of somebody you have killed so far
        list.put("AlexaGrace", "28");
        list.put("LucieWilde", "27");
        list.put("Ariana", "30");
        list.put("Khalifa", "20");
 
        // Getting a Set of Key-value pairs
        Set entrySet = list.entrySet();
 
        // Obtaining an iterator for the entry set
        Iterator it = entrySet.iterator();
 
        // Iterate through HashMap entries(Key-Value pairs)
        System.out.println("PEOPLE YOU HAVE KILLED: ");
        while (it.hasNext()) {
            Map.Entry me = (Map.Entry) it.next();
            System.out.println("Victim names: " + me.getKey()
                    + " ; "
                    + " Age: " + me.getValue());
        }
        //Add a new victim/age key/value into the hashmap.
        System.out.println("Would you like to add a new victim?  Type YES or NO: ");
        String a = in.next();
 
        if (a.equalsIgnoreCase("yes")) {
            System.out.println("Enter a new name: ");
            String name = in.next();
            System.out.println("Enter a new age: ");
            String age = in.next();
            list.put(name, age);
        } else if (a.equalsIgnoreCase("no")) {
        }
 
        //Revive a victim from the hashmap.
        System.out.println("Would you like to revive a victim?  Type YES or NO: ");
        String c = in.next();
 
        if (c.equalsIgnoreCase("yes")) {
            System.out.println("Revive a victim:");
            String name = in.next();
            list.remove(name);
        } else if (c.equalsIgnoreCase("no")) {
 
        }
 
        System.out.println("Would you like to speed up aging proccess of some more victim?  Type YES or NO: ");
 
        String e = in.next();
 
        if (e.equalsIgnoreCase("yes")) {
            System.out.println("Who is the victim ? ");
            String name3 = in.next();
            System.out.println("Enter a new time: ");
            String age3 = in.next();
            list.put(name3, age3);
        } else if (a.equalsIgnoreCase("no")) {
 
        }
 
        // Obtaining an iterator for the entry set
        Iterator it1 = entrySet.iterator();
 
        // Iterate through HashMap entries(Key-Value pairs)
        System.out.println("UPDATED VICTIM'S AGES: ");
        while (it1.hasNext()) {
            Map.Entry me = (Map.Entry) it1.next();
            System.out.println("Victim's name: " + me.getKey()
                    + " ; "
                    + " Grade: " + me.getValue());
        }
    }
}
