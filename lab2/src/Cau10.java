public class Cau09 {
    public static void main(String [] args) {
        int myInt = 9;
        double myDouble = myInt;
        System.out.println(myInt);
        System.out.println(myDouble);

        float myFloat = 1.7f;
        int myInt2 = (int) myFloat;
        System.out.println(myFloat);
        System.out.println(myInt2);
    }
}