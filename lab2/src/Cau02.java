import java.text.DecimalFormat;
import java.util.Scanner;
 
public class Cau02 {
 
    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);
        
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
         
        System.out.println("Nhập vào số thứ nhất: ");

        // nhập vào không phải là số thì sao?
        // không phải số thì nhập lại
        int firstNumber = sc.nextInt();
        System.out.println("Nhập vào số thứ hai: ");
        int secondNumber = sc.nextInt();
         
        int tong = firstNumber + secondNumber;
        System.out.println(firstNumber + " + " + secondNumber + " = " + tong);
         
        int hieu = firstNumber - secondNumber;
        System.out.println(firstNumber + " - " + secondNumber + " = " + hieu);
         
        int tich = firstNumber * secondNumber;
        System.out.println(firstNumber + " * " + secondNumber + " = " + tich);
         
        float thuong = (float) firstNumber / secondNumber;
        System.out.println(firstNumber + " / " + secondNumber + " = " + 
                decimalFormat.format(thuong));  // làm tròn thương đến 2 chữ số thập phân
    }
 
}