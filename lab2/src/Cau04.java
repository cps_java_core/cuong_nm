import java.util.Scanner;
import java.text.DecimalFormat;

public class Cau04 {

    private static Scanner scanner = new Scanner(System.in);

    public static void main (String[] args) {
        DecimalFormat decimalFormat = new DecimalFormat("#.##");

        System.out.print("Nhập tử số, a = ");
        int a = scanner.nextInt();
        System.out.print("Nhập mẫu số, b = ");
        int b = scanner.nextInt();
        float c = (float) a / b;
        System.out.println(a + " / " + b + " = " + 
                decimalFormat.format(c));
        System.out.println(c);

    }
}