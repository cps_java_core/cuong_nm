public class Lab02 {
    public static void main(String[] args) throws Exception {
        System.out.println("HI ALL! WELCOME TO CODE P SOFT GROUP!");
        Cau02();
    }

    public static void cau2(){
        Scanner sc = new Scanner(System.in);
        
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
         
        System.out.println("Nhập vào số thứ nhất: ");

        // nhập vào không phải là số thì sao?
        // không phải số thì nhập lại
        do {
            System.out.println("Nhập vào số thứ nhất: ");
            String firstNumber = sc.next();
            System.out.println("Nhập vào số thứ hai: ");
            String secondNumber = sc.next();
            if(isNumberic(firstNumber) == true && isNumberic(secondNumber ) == true) {
                // var temp1 = Integer.parseInt(firstNumber);
                // var temp2 = Integer.parseInt(secondNumber);

                break;
            }
            
            System.out.println("Ban nhap sai kieu du lieu roi!!, Xin hay nhap lai");
        }while(true);
        
        int temp1 = Integer.parseInt(firstNumber);
        int temp2 = Integer.parseInt(secondNumber);

        int tong = temp1 + temp2;
        System.out.println(temp1 + " + " + temp2 + " = " + tong);
         
        int hieu = temp1 - temp2;
        System.out.println(temp1 + " - " + temp2 + " = " + hieu);
         
        int tich = temp1 * temp2;
        System.out.println(temp1 + " * " + temp2 + " = " + tich);
         
        float thuong = (float) temp1 / temp2;
        System.out.println(temp1 + " / " + temp2 + " = " + 
                decimalFormat.format(thuong));  // làm tròn thương đến 2 chữ số thập phân
    }

    public static void Cau04 {
        private static Scanner scanner = new Scanner(System.in);
        DecimalFormat decimalFormat = new DecimalFormat("#.##");

        System.out.print("Nhập tử số, a = ");
        int a = scanner.nextInt();
        System.out.print("Nhập mẫu số, b = ");
        int b = scanner.nextInt();
        float c = (float) a / b;
        System.out.println(a + " / " + b + " = " + 
                decimalFormat.format(c));
        System.out.println(c);
    }
    
    public static void Cau08{
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập vào số: ");
        int myNumber = sc.nextInt();
        System.out.println(myNumber);
        myNumber +=1;
        System.out.println(myNumber);
    }

    public static void Cau10 {
        int myInt = 9;
        double myDouble = myInt;
        System.out.println(myInt);
        System.out.println(myDouble);

        float myFloat = 1.7f;
        int myInt2 = (int) myFloat;
        System.out.println(myFloat);
        System.out.println(myInt2);
    }

    public static void Cau11 {
        System.out.print("Nhập hệ số bậc 2, a = ");
        float a = scanner.nextFloat();
        System.out.print("Nhập hệ số bậc 1, b = ");
        float b = scanner.nextFloat();
        float f = a * a + b;
        System.out.println(f);
    }

    public static boolean isNumberic(String str) {
        try {  
            Double.parseDouble(str);  
            return true;
          } catch(NumberFormatException e){  
            return false;  
          }  
    }
}
